<?php
ini_set('display_errors', 1);

include_once 'lib/laravel/autoload.php';
include_once 'myEloquent.php';
include_once 'Ciudad.php';
include_once '../config/settings.inc.php';

use \Illuminate\Database\Capsule\Manager as Capsule;

$eloquent = new Capsule;
$eloquent->addConnection(array(
    'driver'    => 'mysql',
    'host'      => _DB_SERVER_,
    'database'  => _DB_NAME_,
    'username'  => _DB_USER_,
    'password'  => _DB_PASSWD_,
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => _DB_PREFIX_
));

$eloquent->setAsGlobal();
$eloquent->bootEloquent();

$url = "http://sandbox.coordinadora.com/ags/1.4/server.php?wsdl";
$cliente = new SoapClient($url, array("trace" => 1, "exception" => 0)); 

$ciudades = $cliente->Cotizador_ciudades();

foreach ($ciudades->Cotizador_ciudadesResult->item as $c){
    $ciudad = Ciudad::where("codigo", $c->codigo)->first();
    
    if (!sizeof($ciudad)){
        $ciudad = new Ciudad();
    }
    
    $ciudad->codigo = $c->codigo;
    $ciudad->nombre = str_replace("_", " ", $c->nombre);
    $ciudad->abreviado = $c->abreviado;
    $ciudad->tipo_poblacion = $c->tipo_poblacion;
    $ciudad->codigo_depto = $c->codigo_departamento;
    $ciudad->nombre_depto = $c->nombre_departamento;
    $ciudad->estado = $c->estado;
    
    $ciudad->save();
    
    echo "Guardada la ciudad ".$c->nombre."<br />";
}

$eloquent::statement("update "._DB_PREFIX_."ciudad a, "._DB_PREFIX_."depto_dane b, "._DB_PREFIX_."state c 
    set a.id_depto = c.id_state 
    where a.codigo_depto = b.codigo_depto 
    and b.iso_code = c.iso_code"
);
