create table pc2015_ciudad(
    id_ciudad int not null auto_increment primary key,
    codigo varchar(10) not null unique,
    nombre varchar(200) not null,
    abreviado varchar(20),
    tipo_poblacion varchar(10),
    codigo_depto varchar(2),
    nombre_depto varchar(100),
    estado varchar(5),
    id_depto int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

CREATE TABLE pc2015_depto_dane (
    codigo_depto VARCHAR(2) NOT NULL,
    iso_code VARCHAR(10) NOT NULL,
    name VARCHAR(10) NOT NULL,
    PRIMARY KEY (codigo_depto, iso_code)
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;

INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('05', 'ANT', 'Antioquia');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('08', 'ATL', 'Atlantico');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('13', 'BOL', 'Bolivar');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('15', 'BOY', 'Boyaca');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('17', 'CAL', 'Caldas');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('18', 'CAQ', 'Caqueta');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('19', 'CAU', 'Cauca');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('20', 'CES', 'Cesar');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('23', 'COR', 'Cordoba');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('25', 'CUN', 'Cundinamarca');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('27', 'CHO', 'Choco');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('41', 'HUI', 'Huila');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('44', 'LAG', 'La Guajira');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('47', 'MAG', 'Magdalena');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('50', 'MET', 'Meta');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('52', 'NAR', 'Nariño');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('54', 'NSA', 'Norte de Santander');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('63', 'QUI', 'Quindio');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('66', 'RIS', 'Risaralda');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('68', 'SAN', 'Santander');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('70', 'SUC', 'Sucre');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('73', 'TOL', 'Tolima');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('76', 'VAC', 'Valle del Cauca');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('81', 'ARA', 'Arauca');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('85', 'CAS', 'Casanare');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('86', 'PUT', 'Putumayo');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('88', 'SAP', 'San Andres');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('91', 'AMA', 'Amazonas');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('94', 'GUA', 'Guainia');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('95', 'GUV', 'Guaviare');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('97', 'VAU', 'Vaupes');
INSERT INTO pc2015_depto_dane (codigo_depto, iso_code, name) VALUES ('99', 'VID', 'Vichada');

create table pc2015_ubicacion_tienda(
    id_ubicacion integer not null primary key auto_increment,
    nombre varchar(100),
    direccion varchar(300),
    telefono varchar(50),
    latitud numeric(10,7),
    longitud numeric(10,7),
    centro enum('S', 'N') default 'N'
)
COLLATE='utf8_general_ci' ENGINE=InnoDB;
