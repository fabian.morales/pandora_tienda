{if $cats|@count > 0}
<ul class="lista-cat">
    {foreach from=$cats item=cat}
    {if $cat.hijas|@count > 0}
    <li class="padre">
    {else}
    <li>
    {/if}
        <a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">
            {$cat.name|escape:'html':'UTF-8'}
        </a>
        {include file="./categorias-arbol.tpl" cats=$cat.hijas}
    </li>
    {/foreach}
</ul>
{/if}