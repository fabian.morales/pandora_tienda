{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{hook h='displayInnerHeader'}
{if isset($category)}
    <div class="container">
        {if $category->id AND $category->active}
            {if $scenes || $category->description || $category->id_image}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content_scene_cat">
                            {if $scenes}
                                <div class="content_scene">
                                    <!-- Scenes -->
                                    {include file="$tpl_dir./scenes.tpl" scenes=$scenes}
                                    {if $category->description}
                                        <div class="cat_desc rte">
                                        {if Tools::strlen($category->description) > 350}
                                            <div id="category_description_short">{$description_short}</div>
                                            <div id="category_description_full" class="unvisible">{$category->description}</div>
                                            <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                                        {else}
                                            <div>{$category->description}</div>
                                        {/if}
                                        </div>
                                    {/if}
                                </div>
                            {else}
                                <!-- Category image -->
                                <div class="content_scene_cat_bg"{if $category->id_image} style="background:url({$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}) right center no-repeat; background-size:cover; min-height:{$categorySize.height}px;"{/if}>
                                    {if $category->description}
                                        <div class="cat_desc">
                                        <span class="category-name">
                                            {strip}
                                                {$category->name|escape:'html':'UTF-8'}
                                                {if isset($categoryNameComplement)}
                                                    {$categoryNameComplement|escape:'html':'UTF-8'}
                                                {/if}
                                            {/strip}
                                        </span>
                                        {if Tools::strlen($category->description) > 350}
                                            <div id="category_description_short" class="rte">{$description_short}</div>
                                            <div id="category_description_full" class="unvisible rte">{$category->description}</div>
                                            <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                                        {else}
                                            <div class="rte">{$category->description}</div>
                                        {/if}
                                        </div>
                                    {/if}
                                 </div>
                              {/if}
                        </div>
                    </div>
                </div>
            {/if}

            {*<div class="row">
                <div class="col-sm-12">
                    <h1 class="page-heading{if (isset($subcategories) && !$products) || (isset($subcategories) && $products) || !isset($subcategories) && $products} product-listing{/if}"><span class="cat-name">{$category->name|escape:'html':'UTF-8'}{if isset($categoryNameComplement)}&nbsp;{$categoryNameComplement|escape:'html':'UTF-8'}{/if}</span>{include file="$tpl_dir./category-count.tpl"}</h1>
                    {if isset($subcategories)}
                        {if (isset($display_subcategories) && $display_subcategories eq 1) || !isset($display_subcategories) }
                            <!-- Subcategories -->
                            <div id="subcategories">
                                <p class="subcategory-heading">{l s='Subcategories'}</p>
                                <ul class="clearfix">
                                    {foreach from=$subcategories item=subcategory}
                                    <li>
                                        <div class="subcategory-image">
                                            <a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">
                                                {if $subcategory.id_image}
                                                    <img class="replace-2x" src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'medium_default')|escape:'html':'UTF-8'}" alt="" width="{$mediumSize.width}" height="{$mediumSize.height}" />
                                                {else}
                                                    <img class="replace-2x" src="{$img_cat_dir}{$lang_iso}-default-medium_default.jpg" alt="" width="{$mediumSize.width}" height="{$mediumSize.height}" />
                                                {/if}
                                            </a>
                                        </div>
                                        <h5><a class="subcategory-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'}</a></h5>
                                        {if $subcategory.description}
                                            <div class="cat_desc">{$subcategory.description}</div>
                                        {/if}
                                    </li>
                                    {/foreach}
                                </ul>
                            </div>
                        {/if}
                    {/if}
                </div>
            </div>*}

            <div class="row">
                <div class="col-md-4">
                    {include file="./categorias-arbol.tpl" cats=$categorias}
                    <img src="{$base_dir}themes/pandora/imagenes/follow.png" class="img-responsive" />
                    <ul class="bloque redes med" style="margin: 0 auto; width: 35%;">
                        <li><a href="https://www.facebook.com/pandorascode/" class="red facebook" target="_blank"></a></li>
                        <li><a href="http://instagram.com/pandorascode" class="red instagram" target="_blank"></a></li>
                        <li><a href="https://twitter.com/pandoras_code" class="red twitter" target="_blank"></a></li>
                    </ul>
                    <div class="filtros categoria">
                        <p>Filtrar</p>
                        <form id="form_filtros" action="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" method="post" class="filtro atributos text-center">
                            {foreach from=$atributos item=atributo}
                                <select name="atributo[{$atributo.atributo.id_attribute_group}]">
                                    <option value="">{$atributo.atributo.name}</option>
                                    {foreach from=$atributo.valores item=valor}
                                        <option value="{$valor.id_attribute}" {if $atributo.sel == $valor.id_attribute}selected{/if} >{$valor.name}</option>    
                                    {/foreach}
                                </select>
                            {/foreach}
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    {if $products}
                        {*<div class="row">
                            <div class="col-sm-12">
                                <div class="content_sortPagiBar clearfix">
                                    <div class="sortPagiBar clearfix">
                                        {include file="./product-sort.tpl"}
                                        {include file="./nbr-product-page.tpl"}
                                    </div>
                                    <div class="top-pagination-content clearfix">
                                        {include file="./product-compare.tpl"}
                                        {include file="$tpl_dir./pagination.tpl"}
                                    </div>
                                </div>                    
                            </div>
                        </div>*}
                        {include file="./product-list.tpl" products=$products}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="content_sortPagiBar">
                                    <div class="bottom-pagination-content clearfix">
                                        {*include file="./product-compare.tpl" paginationId='bottom'*}
                                        {include file="./pagination.tpl" paginationId='bottom'}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
            
        {elseif $category->id}
            <div class="row">
                <div class="col-sm-12">
                    <p class="alert alert-warning">{l s='This category is currently unavailable.'}</p>
                </div>
            </div>
        {/if}
    </div>
{/if}
