{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{hook h='displayInnerHeader'}
{capture name=path}{l s='My account'}{/capture}
<div class="container">
    <div class="row">
        <div class="col-lg-12 caja gris">
            <h1 class="page-heading">
                {l s='My account'}
            </h1>
            <div class="caja blanca">
                {if isset($account_created)}
                    <p class="alert alert-success">
                        {l s='Your account has been created.'}
                    </p>
                {/if}
                <p class="info-account">{l s='Welcome to your account. Here you can manage all of your personal information and orders.'}</p>
                <div class="row">
                    {if $has_customer_an_address}
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a class="boton-cuenta" href="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" title="{l s='Add my first address'}"><i class="icon-building"></i><span>{l s='Add my first address'}</span></a>
                        </div>
                    {/if}
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                        <a class="boton-cuenta" href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="Mi historial"><i class="icon-list-ol"></i><span>Mi Historial</span></a>
                    </div>
                    {if $returnAllowed}
                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                            <a class="boton-cuenta" href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='Merchandise returns'}"><i class="icon-refresh"></i><span>{l s='My merchandise returns'}</span></a>
                        </div>
                    {/if}
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                        <a class="boton-cuenta" href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="Mis Bonos"><i class="icon-file-o"></i><span>Mis Bonos</span></a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                        <a class="boton-cuenta" href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='My Addresses'}"><i class="icon-building"></i><span>{l s='My addresses'}</span></a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                        <a class="boton-cuenta" href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="Mi Perfil"><i class="icon-user"></i><span>Mi Perfil</span></a>
                    </div>

                    {if $voucherAllowed || isset($HOOK_CUSTOMER_ACCOUNT) && $HOOK_CUSTOMER_ACCOUNT !=''}
                        {if $voucherAllowed}
                            <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2">
                                <a class="boton-cuenta" href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='Vouchers'}"><i class="icon-barcode"></i><span>{l s='My vouchers'}</span></a>
                            </div>
                        {/if}
                        {$HOOK_CUSTOMER_ACCOUNT}
                    {/if}
                </div>
                <ul class="footer_links clearfix">
                    <li><a class="boton std gris" href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{l s='Home'}"><span>{l s='Home'}</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


