{hook h='displayInnerHeader' home="1"}
<div class="container">
    <div class="row">
        <div class="col-md-4">
            {include file="./categorias-arbol.tpl" cats=$categorias}
            <img src="{$base_dir}themes/pandora/imagenes/follow.png" class="img-responsive" />
            <ul class="bloque redes med" style="margin: 0 auto; width: 35%;">
                <li><a href="https://www.facebook.com/pandorascode/" class="red facebook" target="_blank"></a></li>
                <li><a href="http://instagram.com/pandorascode" class="red instagram" target="_blank"></a></li>
                <li><a href="https://twitter.com/pandoras_code" class="red twitter" target="_blank"></a></li>
            </ul>
        </div>
        <div class="col-md-8">
            <h1 class="titulo-home2">Nueva colecci&oacute;n</h1>
            {include file="./product-list.tpl" products=$nueva_coleccion}

            <h1 class="titulo-home2">Tu estilo / Insp&iacute;rate</h1>
            {include file="./product-list.tpl" products=$tu_estilo}

            <h1 class="titulo-home2">Lo m&aacute;s top</h1>
            {include file="./product-list.tpl" products=$lo_mas_top}
        </div>
    </div>    
</div>
