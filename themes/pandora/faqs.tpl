{literal}
<script>
(function($, window){
    $(document).ready(function() {
        function scrollTo(oTarget) {
            var alto = 0;

            var ooo = oTarget.offset().top - alto;
            $('html, body').animate({ scrollTop:ooo }, 600);
        }
        
    	$("div.boton_faq").click(function() {
            if ($(this).parent().hasClass('activo')){
                $(".acordeon.activo").removeClass('activo');
            }
            else{
                $(".acordeon.activo").removeClass('activo');
                var $div = $(this).parent();
                $div.addClass('activo');
                setTimeout(function() {
                    scrollTo($div);
                }, 300);
            }
        });
    });
})(jQuery, window);
</script>
{/literal}
{hook h='displayInnerHeader'}
<div class="container">
    <div class="row">
        <div class="col-lg-12 caja blanca">
            <br />
            <h1 class="titulo">Preguntas Frecuentes</h1>
            <br />
            
            <div class="acordeon activo">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Qué significa tener una cuenta de TU PANDORASCODE?</strong>
            </div>
            <div>
                <p>Tu cuenta TUPANDORASCODE nos permite obtener los datos necesarios para el manejo de tus pedidos. 
                    Es importante aclarar que tienes absoluto control sobre tus datos personales y los 
                    puedes modificar en cualquier momento.</p>
                <p>Accede a tu cuenta para:</p>
                <ul>
                    <li>Consultar y modificar tus datos</li>
                    <li>Consultar y modificar tus direcciones de envío</li>
                    <li>Modificar tu contraseña</li>
                    <li>Suscribirte al boletín de noticias y blog</li>
                    <li>Consultar tu historial de pedidos y dream box</li>
                </ul>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo crear mi cuenta de PANDORASCODE?</strong>
            </div>
            <div>
                <p>Para abrir una cuenta: Haz clic en la sección "log in" que se muestra en la parte superior. 
                    A continuación, sigue el procedimiento y rellena el formulario. Si llegas a presentar dificultades,
                    no dudes en ponerte en contacto con nosotros para colaborarte.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Qué pasa si olvido mi usuario y contraseña?</strong>
            </div>
            <div>
                <p>Si olvidaste la contraseña de acceso, haz clic en "log in" en la parte superior y en "¿Olvidó la contraseña?". 
                    Te enviaremos una nueva contraseña de forma automática a tu email.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Es posible recibir información periódica en mi email con las últimas novedades y ofertas?</strong>
            </div>
            <div>
                <p>Sí, claro. Simplemente una vez te hayas registrado, recibirás información sobre las últimas novedades, lookbook, 
                    nuevos productos, y eventos de PANDORASCODE.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo puedo ponerme en contacto con PANDORASCODE?</strong>
            </div>
            <div>
                <p>El servicio de atención al cliente está a tu disposición en el botón ¿DUDAS? o "CONTACTO". 
                    Llámanos o rellena el formulario y te contestaremos en la mayor brevedad posible.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cómo se mi talla?</strong>
            </div>
            <div>
                <p>Puedes consultar nuestro cuadro de tallas, esta funciona para determinar tu talla correcta. 
                    Como la talla y el corte pueden variar, úsalo como guía general para comparar las diferentes 
                    escalas de tallas.</p>
                <p>Estas medidas son de Estados Unidos. Si llegas a tener alguna pregunta especifica de nuestros 
                    productos comunícate con nosotros.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Los precios incluyen el IVA?</strong>
            </div>
            <div>
                <p>El IVA (16%) está incluido en el precio de los artículos disponibles en nuestro Website.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Acabo de hacer mi pedido. ¿Puedo agregar otro artículo?</strong>
            </div>
            <div>
                <p>Desafortunadamente, no podemos agregar artículos a un pedido existente.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Es posible hacer una devolución o cambio de producto?</strong>
            </div>
            <div>
                <p>Para ver nuestra Política completa de devoluciones y cambios, haz clic <a href="{$base_dir}content/3-terminos-y-condiciones-de-uso">aquí.</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Puedo devolver o cambiar artículos que compré en descuento?</strong>
            </div>
            <div>
                <p>Todos los artículos en liquidación son de venta final y no se aceptan devoluciones o cambios. 
                    Para ver nuestra Política completa de devoluciones y cambios, haz clic <a href="{$base_dir}content/3-terminos-y-condiciones-de-uso">aquí.</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Cuándo se enviará mi pedido?</strong>
            </div>
            <div>
                <p>La mayoría de los pedidos se envía dentro de los 5 días hábiles posteriores a la compra. 
                    Los tiempos de entrega se realizaran entre 30 días laborales después de confirmada su compra. 
                    Para ver nuestra Política completa de entrega, haz clic <a href="{$base_dir}content/3-terminos-y-condiciones-de-uso">aquí.</a></p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Dónde está la confirmación de mi pedido?</strong>
            </div>
            <div>
                <p>En cuanto tu pedido se envíe, recibirás una confirmación por correo electrónico a la dirección 
                    que escribiste en tu pedido. Si por algún motivo, no recibiste un correo electrónico, revisa 
                    tu carpeta de correo no deseado.</p>
                <p>También puedes verificar el estado del pedido iniciando sesión en tu cuenta TUPANDORASCODE en nuestro sitio web.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>Me encanta algo que está fuera de stock o no esta mi talla. ¿Qué puedo hacer?</strong>
            </div>
            <div>
                <p>Una vez que un artículo se agota su existencia, rara vez reponemos existencias. Pero siempre 
                    estamos buscando prendas increíbles y muchas veces traemos estilos similares que estamos seguros
                    te encantarán.</p>
            </div>
            
            <div class="acordeon">
                <div class="boton_faq">&nbsp;</div>
                <strong>¿Tienen ubicaciones de tiendas?</strong>
            </div>
            <div>
                <p>¡Sí, en Internet! Somos solamente una tienda de moda en línea, lo que significa que puedes 
                    comprar las 24 horas los 7 días de la semana. Siempre estaremos a tu completa disposición.</p>
            </div>
        </div>
    </div>
</div>


