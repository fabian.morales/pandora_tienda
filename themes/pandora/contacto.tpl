{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{hook h='displayInnerHeader'}
<div class="container">
    <div class="row">
        <div class="col-lg-12 caja gris">
            <br />
            <form id="form_contacto" method="post" class="caja blanca" action="{$base_dir}index.php?controller=custom&tarea=enviarContacto">
                <h3 class="page-subheading big">Contacto</h3>
                
                <div class="row">
                    <div class="col-md-3 col-lg-2"><label for="nombre">Nombre y apellido</label></div>
                    <div class="col-md-9 col-lg-10"><input type="text" id="nombre" name="nombre" /></div>
                </div>
                
                <div class="row">
                    <div class="col-md-3 col-lg-2"><label for="email">E-mail</label></div>
                    <div class="col-md-9 col-lg-10"><input type="email" id="email" name="email" /></div>
                </div>
                
                <div class="row">
                    <div class="col-md-3 col-lg-2"><label for="telefono">Tel&eacute;fono</label></div>
                    <div class="col-md-9 col-lg-10"><input type="text" id="telefono" name="telefono" /></div>
                </div>
                
                <div class="row">
                    <div class="col-md-3 col-lg-2"><label for="asunto">Asunto</label></div>
                    <div class="col-md-9 col-lg-10"><input type="text" id="asunto" name="asunto" /></div>
                </div>
                
                <div class="row">
                    <div class="col-md-3 col-lg-2"><label for="comentarios">Comentarios</label></div>
                    <div class="col-md-9 col-lg-10"><textarea id="comentarios" name="comentarios"></textarea></div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12"><input type="submit" class="boton std negro" value="Enviar" /></div>
                </div>
            </form>
        </div>
    </div>
</div>
        