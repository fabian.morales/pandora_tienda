<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div id="image-block" class="clearfix">
                {if isset($imagenes) && count($imagenes) > 0 }
                    <div class="row">
                        {foreach from=$imagenes item=image name=thumbnails key=k}
                            {assign var=imageIds value="`$producto->id`-`$image.id_image`"}
                            {if !empty($image.legend)}
                                {assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
                            {else}
                                {assign var=imageTitle value=$producto->name|escape:'html':'UTF-8'}
                            {/if}

                            {if $smarty.foreach.thumbnails.index == 0 || $smarty.foreach.thumbnails.index == 1}
                                <div class="col-xs-12 col-md-6">
                                    <a href="{$link->getImageLink($producto->link_rewrite, $imageIds, 'pan_big')|escape:'html':'UTF-8'}" rel="img_gal">
                                        <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($producto->link_rewrite, $imageIds, 'pan_std')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                                    </a>
                                </div>
                            {/if}

                            {if $smarty.foreach.thumbnails.index >= 2 && count($images) > 2}
                                <div class="col-xs-12 col-md-3">
                                    <a href="{$link->getImageLink($producto->link_rewrite, $imageIds, 'pan_big')|escape:'html':'UTF-8'}" rel="img_gal">
                                        <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($producto->link_rewrite, $imageIds, 'pan_small')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                                    </a>
                                </div>
                            {/if}

                        {/foreach}
                    </div>
                {/if}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="nombre detalle_producto">{$producto->name|escape:'html':'UTF-8'}</div>
            <hr />
            <div class="precio_ajax">Col ${$producto->getPrice(true, $smarty.const.NULL, 6)|round:2}</div>
            <br />
            <a class="boton verde" href="{$link->getProductLink($producto)|escape:'html':'UTF-8'}">Ver m&aacute;s</a>
        </div>
    </div>
</div>
