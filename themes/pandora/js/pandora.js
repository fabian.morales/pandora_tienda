(function ($, window) {
    function obtenerCiudades() {
        $.ajax({
            url: 'index.php?controller=custom&tarea=obtener_ciudades',
            data: {id_depto: $("#id_state").val()},
            method: 'post',
            dataType: 'json',
            success: function (json) {
                $("#city").val('');
                $("#city").html('');

                $.each(json, function (i, o) {
                    var $selected = '';

                    if ((o.codigo + '-' + o.nombre) === $("#city_old").val()) {
                        $selected = ' selected="selected"';
                    }

                    $("#city").append('<option value="' + o.codigo + '-' + o.nombre + '"' + $selected + '>' + o.codigo + '-' + o.nombre + '</option>');

                    if ((o.codigo + '-' + o.nombre) === $("#city_old").val()) {
                        $("#city").val(o.codigo + '-' + o.nombre);
                    }

                    $.uniform.update("#city");
                });
            }
        });
    }

    $(document).ready(function () {

        $("#form_filtros select").change(function (e) {
            e.preventDefault();
            $("#form_filtros").submit();
        });

        $("a[rel=img_gal]").fancybox();
        $("a[rel=img_gal_zoom]").fancybox();
        $("a[rel=modal]").fancybox();

        /*$("#banner_home").lightSlider({
         item: 1,
         controls: true,
         pauseOnHover: true,
         auto: true,
         pager: true
         });*/

        $('#banner_home').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 20,
            pager: true,
            nextText: '',
            prevText: '',
            moveSlides: 1,
            infiniteLoop: myslider_loop,
            autoHover: true,
            auto: myslider_loop,
            speed: parseInt(myslider_speed),
            pause: myslider_pause,
            controls: true
        });

        $("#form_news_banner").submit(function (e) {
            e.preventDefault();
            var $form = $("#newsletter_block_left form");

            if ($form.length > 0) {
                $("#newsletter-input").val($("#news_banner").val());
                $('#newsletter_block_left form input[type=submit]').click();
            }
        });

        $('#news_banner').keypress(function (e) {
            if (e.which === 13) {
                $("#newsletter-input").val($("#news_banner").val());
                $('#newsletter_block_left form input[type=submit]').click();
                return false;    //<---- Add this line
            }
        });

        $('#newsletter-input').keypress(function (e) {
            if (e.which === 13) {
                $('#newsletter_block_left form input[type=submit]').click();
                return false;    //<---- Add this line
            }
        });

        if (typeof ($.jqzoom) != 'undefined') {
            $('a[rel=img_gal_zoom]').jqzoom({
                zoomType: 'innerzoom', //innerzoom/standard/reverse/drag
                zoomWidth: 200, //zooming div default width(default width value is 200)
                zoomHeight: 200, //zooming div default width(default height value is 200)
                xOffset: 10, //zooming div default offset(default offset value is 10)
                yOffset: 0,
                title: false
            });
        }

        $("#form_filtros select").change(function (e) {
            e.preventDefault();
            $("#form_filtros").submit();
        });

        $("form#add_address #id_state").change(function (e) {
            e.preventDefault();
            obtenerCiudades();
        });

        obtenerCiudades();
        //newsletter_block_left
    });
})(jQuery, window);