{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo header -->
<div class="row">
    <div class="col-md-10">        
        <ul class="menu black right">
            <li><a href="http://pandorascode.co/">Home</a></li>
            <li><a href="http://pandorascode.co/index.php?controller=custom&tarea=subhome">Shop</a></li>
            <li><a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=103">Lookbook</a></li>
            <li><a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=104">Prensa</a></li>
            <li><a href="http://pandorascode.co/site/index.php?option=com_my_component&controller=blog&Itemid=105">Blog</a></li>
            <li><a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=106">Contacto</a></li>
        </ul>
    </div>
    <div class="col-md-2">
        <ul class="row">
            <li class="col-sm-4"><a href="https://www.facebook.com/pandorascode/" target="_blank"><img src="{$base_dir}themes/pandora/imagenes/facebook_white.png" /></a></li>
            <li class="col-sm-4"><a href="http://instagram.com/pandorascode" target="_blank"><img src="{$base_dir}themes/pandora/imagenes/instagram_white.png" /></a></li>
            <li class="col-sm-4"><a href="https://twitter.com/pandoras_code" target="_blank"><img src="{$base_dir}themes/pandora/imagenes/twitter_white.png" /></a></li>
        </ul>
    </div>
</div>