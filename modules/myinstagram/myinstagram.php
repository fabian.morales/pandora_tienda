<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;

class MyInstagram extends Module
{
    public function __construct()
    {
        $this->name = 'myinstagram';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
                $this->tab = 'front_office_features';
        else
                $this->tab = 'Blocks';
        $this->version = '2.1.1';
        $this->author = 'Fabian Morales';

        $this->bootstrap = true;
        parent::__construct();	

        $this->displayName = 'My Instagram';
        $this->description = 'Bloque Instagram';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install() &&			
                $this->registerHook('displayHome') &&
        $this->registerHook('header');
    }


    public function uninstall()
    {
        // Delete configuration
        parent::uninstall();
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS(($this->_path).'myinstagram.css', 'all');
    }

    public function hookDisplayHome($params)
    {
        //$this->context->controller->addCSS($this->_path.'style.css', 'all');
        ini_set('display_errors', '1');
        $idUsuario = $this->obtenerIdUsuario("pandorascode");
        $items = $this->obtenerActividadReciente($idUsuario, 6);
        
        $this->smarty->assign(array('items' => $items));
        return $this->display(__FILE__, 'myinstagram.tpl');
    }
      
    
    private function obtenerJson($url){
        $ch = curl_init(); 
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	$result = curl_exec ($ch);	
    	curl_close ($ch);    	
    	return json_decode($result);
    }
    
    private function obtenerIdUsuario($usuario){        
        $url = "https://api.instagram.com/v1/users/search?q=".$usuario."&client_id=5db18bf1745341b1a46cd2827a149e3c&access_token=1619729327.e518b54.80e5e1a5c20a48f4a999d180aad0c0a9";
        $usr = '';
        $json = $this->obtenerJson($url);
       
        if (sizeof($json)){
            foreach ($json->data as $u){
                if ($u->username == $usuario){
                    $usr = $u->id;
                    break;
                }
            }
            /*if (sizeof($json->data[0])){
                $usr = $json->data[0]->id;
            }*/
        }
        
        return $usr;
    }
    
    public function obtenerActividadReciente($idUsuario, $limite){
        $url = "https://api.instagram.com/v1/users/".$idUsuario."/media/recent/?client_id=5db18bf1745341b1a46cd2827a149e3c&access_token=1619729327.e518b54.80e5e1a5c20a48f4a999d180aad0c0a9";
        
        if ($limite){
            $url .= "&count=".$limite;
        }
        
        $items = array();
        $json = $this->obtenerJson($url);
        
        if (sizeof($json)){
            $data = $json->data;
            foreach ($data as $d){
            $item = array("img" => $d->images->thumbnail->url, "link" => $d->link);
                $items[] = $item;
            }
        }
        
        return $items;
    }
}
