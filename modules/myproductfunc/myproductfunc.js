(function($, window){    
    $(document).ready(function() {
        $("a[rel='red']").click(function(e) {
            e.preventDefault();
            var sharing_name = $(this).attr('data-nombre');
            var sharing_url = $(this).attr('data-url');
            var sharing_img = $(this).attr('data-img');
            switch($(this).attr('data-red')){
                case 'twitter':
                    window.open('https://twitter.com/intent/tweet?text=' + sharing_name + ' ' + encodeURIComponent(sharing_url), 'sharertwt', 'toolbar=0,status=0,width=640,height=445');
                    break;
                case 'facebook':
                    window.open('http://www.facebook.com/sharer.php?u=' + sharing_url, 'sharer', 'toolbar=0,status=0,width=660,height=445');
                    break;
            }
        });
    });
})(jQuery, window);
