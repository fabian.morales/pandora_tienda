{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="productinfo">
    {*<a class="my_ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, $smarty.capture.default, false)|escape:'html':'UTF-8'}" rel="nofollow" title="Shop" data-id-product-attribute="{$producto.id_product_attribute|intval}" data-id-product="{$producto.id_product|intval}" data-minimal_quantity="{if isset($producto.product_attribute_minimal_quantity) && $producto.product_attribute_minimal_quantity >= 1}{$producto.product_attribute_minimal_quantity|intval}{else}{$producto.minimal_quantity|intval}{/if}">*}
    <a class="my_ajax_add_to_cart_button" href="{$producto.link|escape:'html':'UTF-8'}" title="{$producto.name|escape:'html':'UTF-8'}" itemprop="url">
        <span>Shop</span>
    </a>
</div>
<div class="productinfo compartir">Compartir</div>
<div class="productinfo">
    <ul class="bloque redes">
        <li><a href="#" rel="red" data-red="facebook" data-url="{$link->getProductLink($producto)|escape:'html':'UTF-8'}" data-img="{$link->getImageLink($producto.link_rewrite, $producto.id_image, 'pan_std')|escape:'html':'UTF-8'}" data-nombre="{$producto.name|escape:'html':'UTF-8'}" class="red facebook"></a></li>
        {*<li><a href="#" rel="red" data-red="instagram" data-url="{$link->getProductLink($producto)|escape:'html':'UTF-8'}" data-img="{$link->getImageLink($producto.link_rewrite, $producto.id_image, 'pan_std')|escape:'html':'UTF-8'}" data-nombre="{$producto.name|escape:'html':'UTF-8'}" class="red instagram"></a></li>*}
        <li><a href="#" rel="red" data-red="twitter" data-url="{$link->getProductLink($producto)|escape:'html':'UTF-8'}" data-img="{$link->getImageLink($producto.link_rewrite, $producto.id_image, 'pan_std')|escape:'html':'UTF-8'}" data-nombre="{$producto.name|escape:'html':'UTF-8'}" class="red twitter"></a></li>
    </ul>
</div>