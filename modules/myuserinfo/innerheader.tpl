<div class="row">
    <div class="col-sm-12 relativo">
        {if $isHome}
            <div class="my slider">
                <div class="my slider">
                    {*<ul id="banner_home" class="bxslider clearfix">
                        <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
                        <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
                        <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
                        <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
                    </ul>*}
                    {hook h="displayBannerInnerHead"}
                </div>
            </div>
        {else}
            <img src="{$base_dir}themes/pandora/imagenes/banner_inner.jpg" class="img-responsive img-fullw" />
            <div class="logo_news">
                <div class="row row-centered">
                    <div class="col-sm-6 col-centered">
                        <img class="img-responsive img-fullw" src="{$base_dir}themes/pandora/imagenes/logo_blanco_big.png" />
                        <form id="form_news_banner" method="post" action="{$base_dir}">
                            <input id="news_banner" type="text" placeholder="Inscr&iacute;bete para recibir lo nuevo de pandorascode" />
                            <input type="submit" style="display: none" value="Suscrbirse" />
                        </form>
                        
                    </div>
                </div>
            </div>
        {/if}
    </div>
</div>
<div class="row userinfo">
    <div class="col-md-4"><a href="{$base_dir}index.php?controller=custom&tarea=subhome">Shop</a></div>
    <div class="col-md-5">
        <img src="{$base_dir}/themes/pandora/imagenes/usuario.png" class="icono_cuenta" />
        {if $logged}
            <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Ver mi cuenta" class="userinfo micuenta" rel="nofollow"><span>{$cookie->customer_firstname}</span></a>
            <a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="Cerrar sesi&oacute;n" class="userinfo micuenta" rel="nofollow"><span>Logout</span></a>
        {else}
            <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Iniciar sesi&oacute;n" class="userinfo login" rel="nofollow">Login</a>
        {/if}
    </div>
    <div class="col-md-1">
        <a href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)|addslashes}" title="Dreambox" rel="nofollow">
            <img src="{$base_dir}/themes/pandora/imagenes/dreambox.png" />
        </a>
    </div>
    <div class="col-md-2 relativo">
        <div class="userinfo menu">
            Menu
            
            <ul>
                {foreach from=$categorias item=cat}
                    <li>
                        <a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">
                            {$cat.name|escape:'html':'UTF-8'}
                        </a>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>
</div>