{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block categories module -->
<div class="categorias-home row">
{foreach from=$categorias item=cat}
	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
    	<div class="cathome-item">
			<a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}" title="{$cat.name|escape:'html':'UTF-8'}" class="img">
			{if $cat.id_category}
				<img class="replace-2x" src="{$link->getCatImageLink($cat.link_rewrite, $cat.id_category, 'large_default')|escape:'html':'UTF-8'}" alt="" />
			{else}
				<img class="replace-2x" src="{$img_cat_dir}default-medium_default.jpg" alt="" />
			{/if}
		    </a>
            <h5><a class="cat-name" href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">{$cat.name|truncate:40:'...'|escape:'html':'UTF-8'|truncate:350}</a></h5>
       	</div>
	</div>
{/foreach}
</div>
