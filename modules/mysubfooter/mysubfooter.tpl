{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo sub-footer -->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-8 localizacion">
                    <img src="{$base_dir}themes/pandora/imagenes/pin.png" />
                    <span class="uppercase">Colombia</span><br /><br />
                    <span>Carrera 13 # 101 - 32</span><br />
                    <span>Bogot&aacute;, Colombia</span><br />
                    {*<span>TEL: (571) 3456789</span><br />*}
                    <span><i class="icon-phone"></i> (57) 314 257 00 72</span><br />
                    <span><i class="icon-envelope"></i> info@pandorascode.co</span><br />
                </div>
                {*<div class="col-md-6 localizacion">
                <img src="{$base_dir}themes/pandora/imagenes/pin.png" />
                <span class="uppercase">USA</span><br /><br />
                </div>*}
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-9">
                    <ul class="menu black right">
                        <li><a href="http://pandorascode.co/">Home</a></li>
                        <li><a href="http://pandorascode.co/index.php?controller=custom&tarea=subhome">Shop</a></li>
                        <li><a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=103">Lookbook</a></li>
                        <li><a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=104">Prensa</a></li>
                        <li><a href="http://pandorascode.co/site/index.php?option=com_my_component&controller=blog&Itemid=105">Blog</a></li>
                        <li><a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=106">Contacto</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <ul class="redes black">
                        <li><a href="https://www.facebook.com/pandorascode/" target="_blank" class="red facebook">&nbsp;</a></li>
                        <li><a href="http://instagram.com/pandorascode" target="_blank" class="red instagram">&nbsp;</a></li>
                        <li><a href="https://twitter.com/pandoras_code" target="_blank" class="red twitter">&nbsp;</a></li>
                    </ul>
                </div>
            </div>
            {hook h="displaySubFooter"}
        </div>
    </div>
    <div class="row text-center copy">
        <span>Copyright 2014-2015&nbsp;</span>
        <img src="{$base_dir}themes/pandora/imagenes/logo.png" class="logo footer" />
        <span class="uppercase">&nbsp;All rights reserved</span>
        &nbsp;<a href="{$base_dir}content/3-terminos-y-condiciones-de-uso" rel="modal">Términos y condiciones</a>
        <div class="abadi right uppercase"><a href="http://www.lulodsgn.com/" target="_blank">BY Lulodsgn SAS</a></div>
    </div>
</div>