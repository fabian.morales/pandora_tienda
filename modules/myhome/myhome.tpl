{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo home -->
<div class="row">
    <div class="col-sm-12">
        <div class="my slider">
            {*<ul id="banner_home" class="bxslider clearfix">
                <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
                <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
                <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
                <li><img src="{$base_dir}themes/pandora/imagenes/banner_home.jpg" class="img-responsive img-fullw" /></li>
            </ul>*}
            {hook h="displayBannerHead"}
        </div>
    </div>
    <div class="col-sm-12">
        <div class="bloque redes black">
            <ul class="redes big black">
                <li><a href="https://www.facebook.com/pandorascode/" target="_blank" class="red facebook">&nbsp;</a></li>
                <li><a href="http://instagram.com/pandorascode" target="_blank" class="red instagram">&nbsp;</a></li>
                <li><a href="https://twitter.com/pandoras_code" target="_blank" class="red twitter">&nbsp;</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="bloques home">
    <div class="row">
        <div class="col-sm-3 bloque">
            <a href="{$base_dir}27-nueva-coleccion">
                <img src="{$base_dir}themes/pandora/imagenes/whatsnew.jpg" class="img-fullw" />
            </a>
        </div>
        <div class="col-sm-3 bloque">
            <a href="{$base_dir}28-tu-estilo-inspirate">
                <img src="{$base_dir}themes/pandora/imagenes/tuestilo.jpg" class="img-fullw" />
            </a>
        </div>
        <div class="col-sm-3 bloque">
            <a href="{$base_dir}29-lo-mas-top">
                <img src="{$base_dir}themes/pandora/imagenes/lomastop.jpg" class="img-fullw" />
            </a>
        </div>
        <div class="col-sm-3 bloque">
            {if $logged}
                <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Ver mi cuenta" rel="nofollow"><img src="{$base_dir}themes/pandora/imagenes/tupandora.jpg" class="img-fullw" /></a>
            {else}
                <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="Iniciar sesi&oacute;n" rel="nofollow"><img src="{$base_dir}themes/pandora/imagenes/tupandora.jpg" class="img-fullw" /></a>
            {/if}
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 bloque">
            <div class="tile lookbooks">
                <a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=103">&nbsp;</a>
                <img src="{$base_dir}themes/pandora/imagenes/lookbook.png" class="img-fullw" />
            </div>
            <div class="tile contacto">
                <a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=106">&nbsp;</a>
                <img src="{$base_dir}themes/pandora/imagenes/contacto.png" class="img-fullw" />
            </div>
        </div>
        <div class="col-md-6 bloque">
            <div class="tile shop">
                <a href="{$base_dir}index.php?controller=custom&tarea=subhome">&nbsp;</a>
                <img src="{$base_dir}themes/pandora/imagenes/shop.png" class="img-fullw" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 bloque">
            <div class="tile prensa">
                <a href="http://pandorascode.co/site/index.php?option=com_content&view=article&id=1&Itemid=104">&nbsp;</a>
                <img src="{$base_dir}themes/pandora/imagenes/prensa.png" class="img-fullw" />
            </div>
        </div>
        <div class="col-md-6 bloque">
            <div class="tile blog">
                <a href="http://pandorascode.co/site/index.php?option=com_my_component&controller=blog&Itemid=105">&nbsp;</a>
                <img src="{$base_dir}themes/pandora/imagenes/blog.png" class="img-fullw" />
            </div>
        </div>
    </div>
</div>