<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/      
class CustomControllerCore extends FrontController
{
    //public $php_self = 'my_contacto';

    
    public function setMedia()
    {
        parent::setMedia();

        if (!$this->useMobileTheme()) {
            //TODO : check why cluetip css is include without js file
            $this->addCSS(array(                
                _THEME_CSS_DIR_.'product_list.css' => 'all'
            ));
        }
        
        //$this->addJS(_THEME_JS_DIR_.'category.js');
    }

    /**
     * Initialize product controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        $tarea = Tools::getValue('tarea');
        
        switch($tarea){
            case "subhome":
                $this->mostrarHome2();
                break;
            case "producto_ajax":
                $this->mostrarVistaRapidaProducto(Tools::getValue('id_producto'));
                break;
            case "mostrarContacto":
                $this->mostrarContacto();
                break;
            case "faqs":
                $this->mostrarFaqs();
                break;
            case "obtener_ciudades":
                $this->obtenerCiudades();
                break;
        }
    }
    
    public function mostrarHome2(){
        $catNuevaCol = new Category(31); //padre: 27
        $catTuEstilo = new Category(32); //padfre: 28
        $catMasTop = new Category(33); //padre: 29
        $vars = array(
            "categorias" => $this->obtenerCategorias(),
            "nueva_coleccion" => $catNuevaCol->getProducts(1, 1, 2, null, null, false, true, true, 2),
            "tu_estilo" => $catTuEstilo->getProducts(1, 1, 2, null, null, false, true, true, 2),
            "lo_mas_top" => $catMasTop->getProducts(1, 1, 2, null, null, false, true, true, 2)
        );
        $this->context->smarty->assign($vars);
        $this->setTemplate(_PS_THEME_DIR_.'home2.tpl');
    }
    
    public function obtenerCategorias($padre){
        $categorias = null;
        $res = array();
        if (!sizeof($padre)){
            $categorias = Category::getCategories((int)($this->context->language->id), true, false, ' and c.level_depth = 2', ' order by category_shop.position asc');
        }
        else{
            $categorias = Category::getChildren($padre["id_category"], (int)($this->context->language->id), true);
        }

        foreach ($categorias as $c){
            $c["hijas"] = $this->obtenerCategorias($c);
            $res[] = $c;
        }

        return $res;
    }
    
    public function mostrarVistaRapidaProducto($idProducto){
        $producto = new Product($idProducto, false, (int)($this->context->language->id));
        $imagenes = $producto->getImages((int)($this->context->language->id));
        $vars = array(
            "producto" => $producto,
            "imagenes" => $imagenes
        );
        $this->context->smarty->assign($vars);
        
        echo $this->context->smarty->fetch(_PS_THEME_DIR_.'product-ajax.tpl');
        exit;
    }
    
    public function mostrarContacto(){
        $this->setTemplate(_PS_THEME_DIR_.'contacto.tpl');
    }
    
    public function mostrarFaqs(){
        $this->setTemplate(_PS_THEME_DIR_.'faqs.tpl');
    }
    
    public function obtenerCiudades() {
        $idDepto = Tools::getValue("id_depto");
        echo json_encode($this->dataListaCiudad($idDepto));
        die();
    }

    public function dataListaCiudad($idDepto) {
        $db = Db::getInstance();
        $sql = "select * from " . _DB_PREFIX_ . "ciudad where id_depto = " . $idDepto;
        return $db->executeS($sql);
    }
}